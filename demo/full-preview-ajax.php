<?php
/**
 * A demo loading a HTML URL preview with AJAX. Including title, description and image.
 */

use Jrswgtr\UrlPreview\Model\UrlPreview;
use Jrswgtr\UrlPreview\Scraper\DocumentScraper;
use Jrswgtr\UrlPreview\Scraper\MultiScraper;
use Jrswgtr\UrlPreview\Scraper\ResultStuffer;
use Jrswgtr\UrlPreview\Scraper\SingleScraper;

use Jrswgtr\UrlPreview\Scraper\Loader\DebugDocumentLoader;

use Jrswgtr\UrlPreview\Scraper\Tag\CanonicalTag;
use Jrswgtr\UrlPreview\Scraper\Tag\DescriptionMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\ImageTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgCanonicalMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgDescriptionMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgImageMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgSiteNameMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgTitleMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\ParagraphTag;
use Jrswgtr\UrlPreview\Scraper\Tag\TitleTag;
use Jrswgtr\UrlPreview\Scraper\Tag\Twitter\TwitterDescriptionMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\Twitter\TwitterImageMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\Twitter\TwitterTitleMetaTag;

require_once '../vendor/autoload.php';

if ( isset( $_POST['url'] ) ):
	
	$documentLoader = new DebugDocumentLoader( dirname( __FILE__, 2 ) . '/var/debug' );
	
	$canonicalUrlScraper = new SingleScraper();
	$canonicalUrlScraper
		->addTag( new CanonicalTag() )
		->addTag( new OgCanonicalMetaTag() )
	;
	
	$siteNameScraper = new SingleScraper();
	$siteNameScraper->addTag( new OgSiteNameMetaTag() );
	
	$titleScraper = new SingleScraper();
	$titleScraper
		->addTag( new OgTitleMetaTag() )
		->addTag( new TwitterTitleMetaTag() )
		->addTag( new TitleTag() )
	;
	
	$descriptionScraper = new SingleScraper();
	$descriptionScraper
		->addTag( new DescriptionMetaTag() )
		->addTag( new OgDescriptionMetaTag() )
		->addTag( new TwitterDescriptionMetaTag() )
		->addTag( new ParagraphTag() )
	;
	
	$imageUrlScraper = new SingleScraper();
	$imageUrlScraper
		->addTag( new OgImageMetaTag() )
		->addTag( new TwitterImageMetaTag() )
		->addTag( new ImageTag() )
	;
	
	$multiScraper = new MultiScraper();
	
	$multiScraper
		->setScraper( 'canonicalUrl', $canonicalUrlScraper )
		->setScraper( 'siteName', $siteNameScraper )
		->setScraper( 'title', $titleScraper )
		->setScraper( 'description', $descriptionScraper )
		->setScraper( 'imageUrl', $imageUrlScraper )
	;
	
	$docScraper = new DocumentScraper( $documentLoader, $multiScraper );
	
	$url = $_POST['url'];
	
	$results = $docScraper->scrape( $url );
	
	$urlPreview    = new UrlPreview( $url );
	$resultStuffer = new ResultStuffer();
	
	$resultStuffer->stuff( $results, $urlPreview );
	
	echo $urlPreview; // $urlPreview->__toString();
	
	die( 0 );

endif; ?>

<html>
<head>

    <title>Jrswgtr UrlPreview demo - Full preview with AJAX</title>

    <link href="./css/demo.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<div class="wrapper">
    <h1>Jrswgtr UrlPreview demo - Full preview with AJAX</h1>
    <form id="preview-form">
        <label>
            <input type="text" name="url" class="url-input" placeholder="Enter URL..."/>
        </label>
        <button type="submit" class="submit">Load preview!</button>
    </form>
    <div id="preview-container"></div>
</div>

<script src="./js/full-preview-ajax.js" type="text/javascript"></script>

</body>
</html>