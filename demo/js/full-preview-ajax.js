(function (  ) {
    const form = document.getElementById( 'preview-form' );

    form.addEventListener( 'submit', function ( e ) {

        e.preventDefault();

        const formData = new FormData( this );

        submitForm( formData )

    } );


    function submitForm( formData )
    {
        const url = formData.get( 'url' );
        if ( url === null || url.length < 4 ) {
            return;
        }

        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            const container = document.getElementById( 'preview-container' );

            if ( this.readyState === 4 && this.status === 200 ) {

                if ( this.responseText.length < 1 ) {
                    container.innerHTML = "<span>no preview data available...</span>";
                    return;
                }

                container.innerHTML = createPreview( this.responseText )
            }

        };

        xhr.open( "POST", "./full-preview-ajax.php" );
        xhr.send( formData );
    }

    function createPreview( responseText )
    {
        const model = JSON.parse( responseText );

        console.log(model);

        return "<a class=\"url-preview\" href=\" " + model.url + "\" target=\"_blank\">" +
            "<img class=\"img\" src=\"" + model.imageUrl + "\" />" +
            "<h3>" + model.title + "</h3>" +
            "<p>" + model.description + "</p>" +
            "<p>" + model.canonicalUrl + "</p>" +
            "</a>";
    }
})();