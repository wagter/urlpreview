<?php

namespace Jrswgtr\UrlPreview\Scraper\Loader;

/**
 * To implement a document loader to load a (HTML) document from a URL.
 *
 * Interface DocumentLoaderInterface
 * @package Jrswgtr\UrlPreview\Loader
 *
 * @author Joris Wagter <http://wagter.net>
 */
interface DocumentLoaderInterface
{
	/**
	 * Try to load a document from a URL.
	 *
	 * @param string $url the URL to load the document from
	 *
	 * @return null|string the document or null if failed
	 */
	function load( string $url ): ?string;
}