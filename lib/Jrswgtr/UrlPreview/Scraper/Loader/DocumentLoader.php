<?php

namespace Jrswgtr\UrlPreview\Scraper\Loader;

/**
 * Use to load a (HTML) document from a URL
 *
 * Class DocumentLoader
 * @package Jrswgtr\UrlPreview\Loader\Document
 *
 * @author Joris Wagter <http://wagter.net>
 */
class DocumentLoader implements DocumentLoaderInterface
{
	/**
	 * {@inheritdoc}
	 */
	function load( string $url ): ?string
	{
		$file = fopen( $url, 'r' );
		
		if ( $file === false ) {
			return null;
		}
		
		$content = '';
		
		while ( ! feof( $file ) ) {
			$content .= fgets( $file, 1024 );
		}
		
		$content = $string = str_replace( [ "\r", "\n" ], ' ', $content);
		
		return strlen( $content ) > 0 ? $content : null;
	}
}