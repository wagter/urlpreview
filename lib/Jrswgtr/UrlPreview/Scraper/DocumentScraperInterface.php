<?php

namespace Jrswgtr\UrlPreview\Scraper;

use Jrswgtr\UrlPreview\Scraper\Map\ResultMap;

interface DocumentScraperInterface
{
	/**
	 * @param string $url
	 *
	 * @return ResultMap
	 */
	public function scrape( string $url ): ResultMap;
}