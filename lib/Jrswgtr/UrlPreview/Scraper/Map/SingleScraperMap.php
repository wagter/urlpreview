<?php

namespace Jrswgtr\UrlPreview\Scraper\Map;

use Jrswgtr\UrlPreview\Scraper\SingleScraperInterface;

/**
 * A map to associate keys with scrapers
 *
 * Class ScraperMap
 * @package Jrswgtr\UrlPreview\Scraper
 */
class SingleScraperMap extends AbstractMap
{
	/**
	 * ScraperMap constructor.
	 *
	 * @param array $map
	 */
	public function __construct( array $map = [] )
	{
		foreach ( $map as $key => $scraper ) {
			$this->put( $key, $scraper );
		}
	}
	
	/**
	 * Put a scraper in the map
	 *
	 * @param string $key
	 * @param SingleScraperInterface $scraper
	 *
	 * @return SingleScraperMap
	 */
	public function put( string $key, SingleScraperInterface $scraper ): SingleScraperMap
	{
		$this->map[ $key ] = $scraper;
		
		if ( ! $this->has( $key ) ) {
			$this->keys[] = $key;
		}
		
		return $this;
	}
	
	/**
	 * Get a scraper from the map
	 *
	 * @param string $key
	 *
	 * @return SingleScraperInterface
	 */
	public function get( string $key ): SingleScraperInterface
	{
		return $this->map[ $key ];
	}
}