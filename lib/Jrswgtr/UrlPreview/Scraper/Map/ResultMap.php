<?php

namespace Jrswgtr\UrlPreview\Scraper\Map;

use Jrswgtr\UrlPreview\Scraper\SingleScraperInterface;

/**
 * A map to associate keys with scrapers
 *
 * Class ScraperMap
 * @package Jrswgtr\UrlPreview\Scraper
 */
class ResultMap extends AbstractMap
{
	/**
	 * ScraperMap constructor.
	 *
	 * @param array $map
	 */
	public function __construct( array $map = [] )
	{
		foreach ( $map as $key => $scraper ) {
			$this->put( $key, $scraper );
		}
	}
	
	/**
	 * Put a result in the map
	 *
	 * @param string $key
	 * @param string $result
	 *
	 * @return ResultMap
	 */
	public function put( string $key, string $result ): ResultMap
	{
		$this->map[ $key ] = $result;
		
		if ( ! $this->has( $key ) ) {
			$this->keys[] = $key;
		}
		
		return $this;
	}
	
	/**
	 * Get a result from the map
	 *
	 * @param string $key
	 *
	 * @return SingleScraperInterface
	 */
	public function get( string $key ): string
	{
		return $this->map[ $key ];
	}
	
	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return json_encode( $this->map );
	}
}