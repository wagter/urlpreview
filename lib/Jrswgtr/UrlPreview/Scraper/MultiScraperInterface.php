<?php

namespace Jrswgtr\UrlPreview\Scraper;

use Jrswgtr\UrlPreview\Scraper\Map\ResultMap;

interface MultiScraperInterface
{
	public function scrape( string $document ): ResultMap;
	
	public function setScraper( string $key, SingleScraperInterface $soloScraper ): MultiScraper;
}