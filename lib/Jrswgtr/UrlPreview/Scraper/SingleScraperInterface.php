<?php

namespace Jrswgtr\UrlPreview\Scraper;


use Jrswgtr\UrlPreview\Scraper\Tag\TagInterface;

/**
 * To implement a Scraper for scraping documents.
 *
 * Interface ScraperInterface
 * @package Jrswgtr\UrlPreview\Scraper
 *
 * @author Joris Wagter <http://wagter.net>
 */
interface SingleScraperInterface
{
	/**
	 * Scrape a document for tags.
	 *
	 * @param string $document the document to scrape for tags
	 *
	 * @return string|null the content of the matched tag or null if no match is found
	 */
	public function scrape( string $document ): ?string;
	
	/**
	 * Add a tag to the scraper.
	 *
	 * @param TagInterface $tag a tag to scrape the document for
	 *
	 * @return SingleScraperInterface
	 */
	public function addTag( TagInterface $tag ): SingleScraperInterface;
}