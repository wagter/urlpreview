<?php

namespace Jrswgtr\UrlPreview\Scraper;

use Jrswgtr\UrlPreview\Scraper\Map\ResultMap;
use Jrswgtr\UrlPreview\Scraper\Map\SingleScraperMap;

/**
 * Class DocumentScraper
 * @package Jrswgtr\UrlPreview\Scraper
 */
class MultiScraper implements MultiScraperInterface
{
	/**
	 * @var SingleScraperMap
	 */
	private $soloScrapers;
	
	/**
	 * DocumentScraper constructor.
	 *
	 * @param SingleScraperMap $soloScrapers
	 */
	public function __construct( SingleScraperMap $soloScrapers = null )
	{
		
		$this->soloScrapers = $soloScrapers !== null
			? $soloScrapers
			: new SingleScraperMap();
	}
	
	/**
	 * @param string $document
	 *
	 * @return ResultMap
	 */
	public function scrape( string $document ): ResultMap
	{
		$resultsMap = new ResultMap();
		
		foreach ( $this->soloScrapers as $key => $scraper ) {
			$result = $scraper->scrape( $document );
			if ( $result !== null ) {
				$resultsMap->put( $key, $result );
			}
		}
		
		return $resultsMap;
	}
	
	/**
	 * @param string $key
	 * @param SingleScraperInterface $soloScraper
	 *
	 * @return MultiScraper
	 */
	public function setScraper( string $key, SingleScraperInterface $soloScraper ): MultiScraper
	{
		$this->soloScrapers->put( $key, $soloScraper );
		
		return $this;
	}
}