<?php

namespace Jrswgtr\UrlPreview\Scraper;

use Jrswgtr\UrlPreview\Scraper\Tag\TagInterface;

/**
 * To scrape documents for tags.
 *
 * Class Scraper
 * @package Jrswgtr\UrlPreview\Scraper
 *
 * @author Joris Wagter <http://wagter.net>
 */
class SingleScraper implements SingleScraperInterface
{
	/**
	 * @var TagInterface[] the tags to scrape the document for
	 */
	private $tags = [];
	
	/**
	 * Scraper constructor.
	 *
	 * @param TagInterface[] $tags the tags to scrape the document for
	 */
	public function __construct( array $tags = [] )
	{
		foreach ( $tags as $tag ) {
			$this->addTag( $tag );
		}
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function scrape( string $document ): ?string
	{
		foreach ( $this->tags as $tag ) {
			
			$match = $tag->match( $document );
			
			if ( $match !== null ) {
				return $match;
			}
		}
		
		return null;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function addTag( TagInterface $tag ): SingleScraperInterface
	{
		$this->tags[] = $tag;
		
		return $this;
	}
}