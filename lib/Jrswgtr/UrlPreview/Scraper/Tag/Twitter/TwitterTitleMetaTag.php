<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\Twitter;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a twitter:image meta tag in a HTML document
 *
 * <meta property="twitter:title" content="Site Title" />
 *
 * Class TwitterTitleMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TwitterTitleMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'twitter:title', $document );
	}
}