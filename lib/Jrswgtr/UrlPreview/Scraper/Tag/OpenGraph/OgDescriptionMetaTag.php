<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a og:description meta tag in a HTML document
 *
 * <meta property="og:description" content="Site description" />
 *
 * Class OgDescriptionMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgDescriptionMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:description', $document );
	}
}