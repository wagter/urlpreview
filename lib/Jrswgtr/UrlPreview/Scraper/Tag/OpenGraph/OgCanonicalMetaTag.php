<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a og:url meta tag in a HTML document
 *
 * <meta property="og:url" content="http://canonical-url.com" />
 *
 * Class OgCanonicalMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgCanonicalMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:url', $document );
	}
}