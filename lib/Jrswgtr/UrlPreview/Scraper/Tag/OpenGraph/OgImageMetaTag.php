<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a og:image meta tag in a HTML document
 *
 * <meta property="og:image" content="http://url-to-image.com/image.jpg" />
 *
 * Class OgImageMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgImageMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:image', $document );
	}
}