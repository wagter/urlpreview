<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag;

class CanonicalTag implements TagInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function match( string $document ): ?string
	{
		preg_match( '/<link href="(.*?)" rel="canonical"/', $document, $match );
		
		if ( count( $match ) < 2 ) {
			preg_match( '/<link rel="canonical" href="(.*?)"/', $document, $match );
		}
		
		if ( count( $match ) < 2 ) {
			return null;
		}
		
		if ( count( $match ) > 1 && is_string( $match[1] ) ) {
			return $match[1];
		} else if ( is_array( $match[1] ) && count( $match[1] ) > 0 ) {
			return $match[1][0];
		}
		
		return null;
	}
}