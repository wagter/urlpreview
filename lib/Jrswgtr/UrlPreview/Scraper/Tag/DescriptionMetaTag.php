<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag;

/**
 * Match a description meta tag in a HTML document
 *
 * <meta name="description" content="Site description" />
 *
 * Class DescriptionMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class DescriptionMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByName( 'description', $document );
	}
}