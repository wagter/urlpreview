<?php

namespace Jrswgtr\UrlPreview\Scraper\Cache;


use Jrswgtr\UrlPreview\Scraper\Map\ResultMap;

interface CacheProviderInterface
{
	function put( string $url, ResultMap $resultMap ): CacheProviderInterface;
	
	function has( string $url ): bool;
	
	function get( string $url ): ResultMap;
	
	function valid( string $url ): bool;
}